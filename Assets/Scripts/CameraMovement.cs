﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    public float speed = 1000;
	
	// Update is called once per frame
	void Update () 
    {
        MoveRight();
    }
    // Moves Camera To Right With Set Speed
    void MoveRight() 
    {
        transform.Translate(Vector3.right * speed * Time.deltaTime);
    }
}
