﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundMoving : MonoBehaviour {
    public float speed = 10;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update()
    {
        MoveLeft();
    }
    // Moves Camera To Right With Set Speed
    void MoveLeft()
    {
        transform.Translate(Vector3.left * speed * Time.deltaTime);
        if (transform.localPosition.x <= -7130)
        {
            transform.localPosition = new Vector3(7130, 0, 10);
        }
    }
}