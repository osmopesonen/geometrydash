﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

    public float JumpForce = -100;
    private Rigidbody2D rb;
    private bool isJumpingUp = false;

    private void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
    }
    void Update () 
    {
        Jump();
	}

    void Jump(){

        if (Input.GetKey(KeyCode.Mouse0) == true && rb.velocity.y == 0)

        {
            rb.velocity = Vector2.up * JumpForce;
            isJumpingUp = true;
            StartCoroutine(RotateMe(new Vector3(0, 0, 180), 0.3f));
        }
        if (rb.velocity.y <= 0 && isJumpingUp == true) 
        {
            isJumpingUp = false;
        }
    }

    IEnumerator RotateMe(Vector3 byAngles, float inTime)
    {
        Quaternion fromAngles = transform.rotation;
        fromAngles.z = fromAngles.z - 0.01f;
        fromAngles.w = fromAngles.w - 0.01f;
        Quaternion toAngles = Quaternion.Euler(transform.eulerAngles + byAngles);
        for(float t = 0; t < 1; t += Time.deltaTime / inTime)
        {
            transform.rotation = Quaternion.Lerp(fromAngles, toAngles, t);
            yield return null;
        }
        transform.rotation = toAngles;
        yield return null;
    }
}
